import React from "react";
import { Link, withRouter, NavLink } from "react-router-dom";
const Navbar = () => {
  return (
    <nav className="nav-wrapper black darken-3">
      <div className="cotainer">
        <Link to="/">Github Clone</Link>
        <ul className="right">
          <li>
            <Link to="/repositoryTab">RepositoryTab</Link>
          </li>
          <li>
            <NavLink to="/overview">Overview</NavLink>
          </li>
          <li>
            <NavLink to="/projects">Projects</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};
export default withRouter(Navbar);
