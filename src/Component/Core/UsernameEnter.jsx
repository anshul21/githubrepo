import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import RepositoryTab from "./Views/RepositoryTab";
import Overview from "./Views/Overview";
import Projects from "./Views/Projects";
import Navbar from "./Navbar";
import UserNameEnter from "./Views/UserNameEnter";
export default function UsernameEnter() {
  return (
    <BrowserRouter>
      <Navbar />
      <Route exact path="/" component={UserNameEnter}></Route>
      <Route path="/repositoryTab" component={() => <RepositoryTab />}></Route>
      <Route path="/overview" component={Overview}></Route>
      <Route path="/projects" component={Projects}></Route>
    </BrowserRouter>
  );
}
