import React, { useState } from "react";
import { connect } from "react-redux";
import SetUserAction from "../../../Actions/SetUserAction";
import "../../../CSS/UsernameEntries.css";
import { useHistory } from "react-router-dom";
const UserNameEnter = (props) => {
  const [state, setstate] = useState("");
  const history = useHistory();
  return (
    <div className="login-page">
      <input
        className="login-page-input"
        type="text"
        placeholder={"Enter Your Github User Name..."}
        value={state}
        onChange={(e) => setstate(e.target.value)}
      />
      <button
        className="login-page-input-button"
        onClick={() => {
          props.setUser(state);
          history.push("/repositoryTab");
        }}
      >
        Check My Repositories
      </button>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  ...SetUserAction(dispatch),
});
export default connect(null, mapDispatchToProps)(UserNameEnter);
