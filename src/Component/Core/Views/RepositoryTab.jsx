import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import RepositoryAction from "../../../Actions/RepositoryAction";
import FetchUserInfo from "../../../Actions/FetchUserInfo";
import List from "./List";
import Caplsule from "./Caplsule";
import USerInfoView from "./USerInfoView";
import "../../../CSS/repositoryTab.css";
import Loader from "react-loader-spinner";
const RepositoryTab = (props) => {
  const [searchText, setSearchText] = useState("");
  const [list, UpdatedList] = useState(props.repositoryTabs);
  useEffect(() => {
    props.get(props.username);
    props.getUserInfo(props.username);
  }, []);

  useEffect(() => {
    UpdatedList(props.repositoryTabs);
  }, [props.repositoryTabs]);

  const filterList = (value) => {
    if (Array.isArray(props.repositoryTabs)) {
      const updatedList = props.repositoryTabs.filter((item) => {
        return item.name.toLowerCase().search(value.toLowerCase()) !== -1;
      });
      UpdatedList(updatedList);
    }
  };
  if (props.loading)
    return (
      <Loader
        className="loader"
        type="PCirclesuff"
        color="#00BFFF"
        height={100}
        width={100}
      />
    );
  if (props.userInfo && props.repositoryTabs)
    return (
      <div className="repo-tab mx-auto row justify-content-md-between container">
        <div className="repo-tab-user-info col-2">
          <USerInfoView userInfo={props.userInfo} />
        </div>
        <div className="repo-tab-list col-6">
          <input
            className="find-repo-search"
            type="search"
            placeholder="Find a repository.."
            value={searchText}
            onChange={(e) => {
              filterList(e.target.value);
              setSearchText(e.target.value);
            }}
          />
          <List
            subCompoent={Caplsule}
            data={list || props.repositoryTabs}
          ></List>
        </div>
      </div>
    );
  if (props.error) return <div>API fetching failed</div>;
  else return <div>Please Go to home page and enter your Username</div>;
};

const mapStateToProps = (state) => ({
  repositoryTabs: state.repodata,
  username: state.currentUser,
  userInfo: state.userInfo,
  loading: !(!state.loadingUserInfo && !state.loadingRepo),
  error: state.error,
});
const mapDispatchToProps = (dispatch) => ({
  ...RepositoryAction(dispatch),
  ...FetchUserInfo(dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(RepositoryTab);
