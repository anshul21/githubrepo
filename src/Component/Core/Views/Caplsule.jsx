import React from "react";
import "../../../CSS/Capsule.css";
export default function Caplsule({ props }) {
  return (
    <div className="border-bottom">
      <a
        className="repo-name mb-1"
        href={`https://github.com/${props.owner.login}/${props.name}`}
      >
        {props.name}
      </a>
      <div className="repo-description mb-2 text-gray  pr-4">
        {props.description}
      </div>
      <div className="repo-language-date-licence mb-1">
        <div className="repo-language">
          <span class="repo-language-color"></span>
          <span>{props.language}</span>
        </div>

        {props.license && (
          <span className="ml-3 mr-3">{props.license.name}</span>
        )}
        <div className="repo-date">
          <span>{` Updated On `}</span>
          <span>{new Date(`${props.updated_at}`).toUTCString()}</span>
        </div>
      </div>
      <br />
    </div>
  );
}
