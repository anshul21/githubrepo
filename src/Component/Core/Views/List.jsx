import React from "react";

export default function List(props) {
  const SubComponent = props.subCompoent;
  return (
    <div>
      {Array.isArray(props.data) &&
        props.data.map((dataOBJ) => {
          return <SubComponent props={dataOBJ}></SubComponent>;
        })}
    </div>
  );
}
