import React from "react";
import Image from "react-bootstrap/Image";
import "../../../CSS/Userinfo.css";
export default function USerInfoView(props) {
  const userInfo = props.userInfo;
  return (
    <div>
      <div>
        <Image
          className="user-info-image"
          src={userInfo && userInfo.avatar_url}
          roundedCircle
        />
        <h1>{userInfo && userInfo.name}</h1>
        <span className="vcard-username">{userInfo && userInfo.login}</span>
        <p className="user-profile-bio mb-3 f-4">{userInfo && userInfo.bio}</p>
        <div>
          <button className="btn-block">Follow</button>
          <span>...</span>
        </div>
        <p>
          {userInfo && userInfo.followers}
          {` Followers   `}
          {userInfo && userInfo.following}
          {` Following`}
        </p>
        <div>{userInfo && userInfo.email}</div>
      </div>
    </div>
  );
}
