const URL = {
  repositoryTabUrl: (username) =>
    `https://api.github.com/users/${username}/repos`,
  UserInfoUrl: (username) => `https://api.github.com/users/${username}`,
};

export default URL;
