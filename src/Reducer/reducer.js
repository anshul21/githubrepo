const defaultState = {
  currentUser: "",
};

function reducer(state = defaultState, action) {
  switch (action.type) {
    case "SET_USER":
      return {
        ...state,
        currentUser: action.payload,
      };
    case "READ_REPO_FETCHED":
      return {
        ...state,
        repodata: action.payload,
        loadingRepo: false,
      };
    case "READ_USERINFO_FETCHED":
      return {
        ...state,
        userInfo: action.payload,
        loadingUserInfo: false,
      };
    case "READ_REPO_START":
      return {
        ...state,
        repodata: null,
        loadingRepo: true,
      };
    case "READ_USERINFO_START":
      return {
        ...state,
        userInfo: null,
        loadingUserInfo: true,
      };
    case "READ_REPO_ERROR":
      return {
        ...state,
        repodata: null,
        loadingRepo: false,
        error: true,
      };
    case "READ_USERINFO_ERROR":
      return {
        ...state,
        userInfo: null,
        loadingUserInfo: false,
        error: true,
      };
    default:
      return state;
  }
}

export default reducer;
