import URL from "../URL";
import axios from "axios";

export default {
  get: (username) => {
    return axios
      .get(URL.repositoryTabUrl(username ? username : "sagar104g"))
      .then((res) => {
        const data = res.data;
        return data;
      });
  },
};
