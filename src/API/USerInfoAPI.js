import URL from "../URL";
import axios from "axios";

export default {
  getUserInfo: (username) => {
    return axios
      .get(URL.UserInfoUrl(username ? username : "sagar104g"))
      .then((res) => {
        const data = res.data;
        return data;
      });
  },
};
