import React from "react";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";

import UsernameEnter from "./Component/Core/UsernameEnter";
function App() {
  return (
    <div className="App">
      <ReactNotification />
      <UsernameEnter />
    </div>
  );
}

export default App;
