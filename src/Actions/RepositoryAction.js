import RepositoryAPI from "../API/RepositoryAPI";
import { store } from "react-notifications-component";
export default (dispatch) => ({
  get: (username) => {
    dispatch({ type: "READ_REPO_START" });
    return RepositoryAPI.get(username)
      .then((res) => {
        dispatch({
          type: "READ_REPO_FETCHED",
          payload: res,
        });
      })
      .catch((err) => {
        store.addNotification({
          title: "Error While Fetching API",
          message: "API fetching Fail",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 10000,
            onScreen: true,
          },
        });
        dispatch({ type: "READ_REPO_ERROR" });
        console.log(err);
      });
  },
});
