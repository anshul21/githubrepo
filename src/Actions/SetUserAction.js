export default (dispatch) => ({
  setUser: (username) => {
    dispatch({
      type: "SET_USER",
      payload: username,
    });
  },
});
