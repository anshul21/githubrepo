import USerInfoAPI from "../API/USerInfoAPI";
import { store } from "react-notifications-component";
export default (dispatch) => ({
  getUserInfo: (username) => {
    dispatch({ type: "READ_USERINFO_START" });
    return USerInfoAPI.getUserInfo(username)
      .then((res) => {
        dispatch({
          type: "READ_USERINFO_FETCHED",
          payload: res,
        });
      })
      .catch((err) => {
        store.addNotification({
          title: "Error While Fetching API",
          message: "API fetching Fail",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 10000,
            onScreen: true,
          },
        });
        dispatch({ type: "READ_USERINFO_ERROR" });
        console.log(err);
      });
  },
});
